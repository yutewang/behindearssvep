% A sample code to run detection of steady-state visual evoked potentials (SSVEPs)
% using the ensemble task-related component analysis (TRCA)-based methods [1] and
% the filter bank canonical correlation analysis (FBCCA) [2].
%
% Dataset (sample.mat):
%   SSVEP data set recorded from a single subject using 40 visual stimuli coded
%   by the joint frequency-phase modulation (JFPM) [3]
%       - Stimulus frequencies          : 8.0 - 15.8 Hz with an interval of 0.2 Hz
%       - Stimulus phases               : 0pi, 0.5pi, 1.0pi, 1.5pi
%       - The number of channels        : 9 (1: Pz, 2: PO5,3:  PO3, 4: POz, 5: PO4, 6: PO6, 7: O1, 8: Oz, and 9: O2)
%       - The number of blocks (Recording sessions) : 6
%       - Data length of epochs         : 5 seconds
%       - Sampling frequency            : 250 Hz
%
% Parameters for the simualtion :
%   dlen_s                              : data length (s) for target identification
%   nFBs                                : the number of sub-bands in filter bank analysis
%   nHarms                              : The number of harmonics in the canonical correlation analysis (CCA)
%   isEnsemble                      : 1: The ensemble TRCA-based method, 0: The TRCA-based method
%
% See also:
%   test_fbcca.m
%   train_trca.m
%   test_trca.m
%   filterbank.m
%   itr.m
%
% Reference:
%   [1] M. Nakanishi, Y. Wang, X. Chen, Y. -T. Wang, X. Gao, and T.-P. Jung,
%       "Enhancing Detection of SSVEPs via Exploiting Inter-trial Information for a High-Speed Brain Speller",
%       , IEEE Trans. Biomed. Eng, Vol., No., 2017. (?)
%   [2] X. Chen, Y. Wang, S. Gao, T. -P. Jung and X. Gao,
%       "Filter bank canonical correlation analysis for implementing a high-speed SSVEP-based brain-computer interface",
%       J. Neural Eng., vol.12, 046008, 2015.
%   [3] X. Chen, Y. Wang, M. Nakanishi, X. Gao, T. -P. Jung, S. Gao,
%       "High-speed spelling with a noninvasive brain-computer interface",
%       Proc. Int. Natl. Acad. Sci. U. S. A, vol. 112, no.44, pp.E6058-6067, 2015.
%
% Masaki Nakanishi, 28-Jul-2016
% Swartz Center for Computational Neuroscience, Institute for Neural
% Computation, University of California San Diego
% E-mail: masaki@sccn.ucsd.edu
% help main

%% Clear workspace
% cd D:\Dropbox\Dropbox\YT\SSVEP\TRCA_code;
clear all
close all

%% Parameter for analysis
filtertype = 'firfir';
dlen_s          = 4.5;
nFBs             = 1;
nHarms         = 1;
isEnsemble   = 0;

%% Fixed parameter
fs                 = 512;                                               % Sampling rate [Hz]
slen_s          = 1;                                                % Duration for gaze shifting [s]
%listFreqs       = [8:1:15 8.2:1:15.2 8.4:1:15.4 8.6:1:15.6 8.8:1:15.8];
listFreqs       = [9:13];
%% Load data
load('./subject1/sbj01_OC.mat');
[nTargs, nChans, nSmpls, nTrials] = size(EEG_sub);
dlen_smpl = round(dlen_s*fs);
EEG_sub = EEG_sub(:, :, 1:dlen_smpl, :);
valTrue = [1:1:nTargs];
elen_s = dlen_s + slen_s;                                       % Epoch length [s]
%% The FBCCA-based method
% 
fprintf('Results of the FBCCA-based method.\n');

for trial_i = 1:1:nTrials
    % Test -----------------------------------------------
    testdata = squeeze(EEG_sub(:, :, :, trial_i));
    estimated = test_fbcca(testdata, listFreqs, fs, nHarms, nFBs,filtertype);
    
    % Evaluation ----------------------------------------
    isCorrect = (estimated==valTrue);
    accs(trial_i) = mean(isCorrect);
    itrs(trial_i) = itr(nTargs, accs(trial_i), elen_s);
    fprintf('Trial %d: Accuracy = %2.2f%%, ITR = %2.2f bits/min\n', trial_i, accs(trial_i)*100, itrs(trial_i));
    
end % trial_i

fprintf('Averaged accuracy = %2.2f%c%2.2f%%, Averaged ITR  = %2.2f%c%2.2f bits/min.\n\n', ...
    mean(accs)*100, char(177), std(accs)*100, mean(itrs), char(177), std(itrs));

%% The ensemble TRCA-based method
fprintf('Results of the ensemble TRCA-based method.\n');

for loocv_i = 1:1:nTrials
    
    % Training phase -----------------------------------
    traindata = EEG_sub;
    traindata(:, :, :, loocv_i) = [];%remove loocv_i trial in the whole dataset
    model = train_trca(traindata, fs, nFBs,filtertype);
    
    % Test phase ---------------------------------------
    testdata = squeeze(EEG_sub(:, :, :, loocv_i));
    estimated = test_trca(testdata, model, isEnsemble,filtertype);
    
    % Evaluation ----------------------------------------
    isCorrect = (estimated==valTrue);
    accs(loocv_i) = mean(isCorrect);
    itrs(loocv_i) = itr(nTargs, accs(loocv_i), elen_s);
    fprintf('Trial %d: Accuracy = %2.2f%%, ITR = %2.2f bits/min\n', loocv_i, accs(loocv_i)*100, itrs(loocv_i));
    
end % loocv_i

fprintf('Averaged accuracy = %2.2f%c%2.2f%%, Averaged ITR  = %2.2f%c%2.2f bits/min.\n\n', ...
    mean(accs)*100, char(177), std(accs)*100, mean(itrs), char(177), std(itrs));

