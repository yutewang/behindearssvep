function y = filterbank(eeg, fs, fb_i)
% Filter bank analysis for decomposing EEG data into sub-band components [1].
%
% function y = filterbank(eeg, fs, fb_i)
%
% Input:
%   eeg             : Input eeg data
%                               (The number of targets, The number of channels, Data length [sample])
%   fs              : Sampling frequency
%   fb_i            :Index of filters in filter bank analysis
%
% Output:
%   y               : Sub-band components decomposed by a filter bank.
%
% Reference:
%   [1] X. Chen, Y. Wang, S. Gao, T. -P. Jung and X. Gao,
%       "Filter bank canonical correlation analysis for implementing a high-speed SSVEP-based brain-computer interface",
%       J. Neural Eng., vol.12, 046008, 2015.
%
% Masaki Nakanishi, 28-Jul-2016
% Swartz Center for Computational Neuroscience, Institute for Neural
% Computation, University of California San Diego
% E-mail: masaki@sccn.ucsd.edu

if nargin < 3 || isempty(fb_i)
    warning('stats:filterbank:MissingInput', 'Index of filters is not defined. Default value (fb_i = 1) will be used.'); 
    fb_i = 1;
end

if ~exist('fs', 'var') || isempty(fs)
    error('stats:filterbank:MissingInput', 'Sampling rate is not defined.'); 
end

[nChans, ~, nTrials] = size(eeg);
fs=fs/2;

fbprm = [6, 4; 14, 10; 22, 16; 30, 24; 38, 32; 46, 40; 54, 48; 62, 56; 70, 64; 78, 72];
Wp = [fbprm(fb_i, 1)/fs, 90/fs];
Ws = [fbprm(fb_i, 2)/fs, 100/fs];

[N, Wn]=cheb1ord(Wp, Ws, 3, 40);
[B, A] = cheby1(N, 0.5, Wn);

y = zeros(size(eeg));
if nTrials == 1
    for ch_i = 1:1:nChans
        y(ch_i, :) = filtfilt(B, A, eeg(ch_i, :));
    end % ch_i
else
    for trial_i = 1:1:nTrials
        for ch_i = 1:1:nChans
            y(ch_i, :, trial_i) = filtfilt(B, A, eeg(ch_i, :, trial_i));
        end % trial_i
    end % ch_i
end