function [ channels ] = getChannelLabel( subjectName, area )

if (subjectName == 2)
     if(strcmp(area, 'behindEar'))
        channels = {'E7' 'E8' 'E10' 'C7' 'C8' 'C10'};
    elseif(strcmp(area,'occipital'))
        channels = {'F26' 'F28' 'F29' 'F30' 'F31' 'F32'};%F27 was dead
    end
else
    if(strcmp(area, 'behindEar'))
        channels = {'E7' 'E8' 'E10' 'C7' 'C8' 'C10'};
    elseif(strcmp(area,'occipital'))
        channels = {'F27' 'F28' 'F29' 'F30' 'F31' 'F32'};
    end
end
end

