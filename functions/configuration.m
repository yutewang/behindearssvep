function config = configuration( deviceName )
if(strcmp(deviceName,'BioSemi1'))%data recorded under /exp/
    config.channels = {'A2' 'A3' 'A4' 'A5' 'A6' 'A7' 'A8' 'A9'};
    config.epochTime = [-0.5+0.7+0.14  4.0+0.7+0.14];
    config.refChannel = 8;
elseif(strcmp(deviceName,'BioSemi2'))%data recorded under /exp2/
    config.channels = {'A1' 'A2' 'A3' 'A4' 'A5' 'A6' 'A7' 'A8'};
    config.epochTime = [-0.5+0.14  1.0+0.14];
    config.refChannel = 8;
elseif(strcmp(deviceName,'Q30'))
    config.channels = {'Fz' 'PO7' 'PO8' 'PO3' 'PO4' 'O1' 'O2'};
    config.epochTime = [-0.5+0.7+0.14  4.0+0.7+0.14];
    config.refChannel = 1;
elseif(strcmp(deviceName,'nGoggle20'))
    config.channels = {'Fz' 'Oz' 'O2' 'Pz' 'P4' 'P3' 'O1'};
    config.epochTime = [-0.5+0.7+0.14  4.0+0.7+0.14];
    config.refChannel = 1;
end

%config = struct('channels', channels, 'epochTime',epochTime,'refChannel', refChannel);
end

