function results = test_fbcca(eeg, listFreqs, fs, nHarms, nFBs,filterType)
% Steady-state visual evoked potentials (SSVEPs) detection using the filter
% bank canonical correlation analysis (FBCCA)-based method [1].
% 
% function [ results ] = test_fbcca(eeg, targets, nam_harm, elen_sec)
%
% Input:
%   eeg             : Input eeg data
%                               (The number of targets, The number of channels, Data length [sample])
%   listFreq        : List for stimulus frequencies
%   fs              : Sampling frequency
%   nHarms          : The number of harmonics
%   nFBs            : The number of filters in filterbank analysis
%
% Output:
%   results         : The target estimated by this method
%
% Reference:
%   [1] X. Chen, Y. Wang, S. Gao, T. -P. Jung and X. Gao,
% 	"Filter bank canonical correlation analysis for implementing a high-speed SSVEP-based brain-computer interface",
% 	J. Neural Eng., vol.12, 046008, 2015.
%
% Masaki Nakanishi, 28-Jul-2016
% Swartz Center for Computational Neuroscience, Institute for Neural
% Computation, University of California San Diego
% E-mail: masaki@sccn.ucsd.edu

if nargin < 3, error('stats:cca_reference:LackOfInput', 'Not enough input arguments.'); end
if ~exist('nHarms', 'var') || isempty(nHarms), nHarms = 3; end
if ~exist('nFBs', 'var') || isempty(nFBs), nFBs = 5; end

[nTargs, ~, nSmpls] = size(eeg);
a = [1:nFBs].^(-1.25)+0.25;
% dataShift = 512*0.5;
filterOrder = 256;
y_ref = cca_reference(listFreqs, fs, nSmpls-filterOrder, nHarms);
for targ_i = 1:1:nTargs
     test_tmp = squeeze(eeg(targ_i, :, :));
     for fb_i = 1:1:nFBs
%          testdata = filterbank(test_tmp, fs, fb_i);
         testdata = myFilter(test_tmp,filterOrder,filterType);
         testdata = testdata(:,filterOrder+1:end);
         for class_i = 1:1:nTargs
             refdata = squeeze(y_ref(class_i, :, :));
             [~,~,r_tmp] = canoncorr(testdata', refdata');
             r(fb_i, class_i) = r_tmp(1,1);
         end % class_i
     end % fb_i
     rho = a*r;
    [~, tau] = max(rho);
     results(targ_i) = tau;
end % targ_i

function [ y_ref ] = cca_reference(listFreq, fs, nSmpls, nHarms)
% Generate reference signals for the canonical correlation analysis (CCA)
% -based steady-state visual evoked potentials (SSVEPs) detection [1, 2].
%
% function [ y_ref ] = cca_reference(listFreq, fs,  nSmpls, nHarms)
% 
% Input:
%   listFreq        : List for stimulus frequencies
%   fs              : Sampling frequency
%   nSmpls          : The number of samples in an epoch
%   nHarms          : The number of harmonics
%
% Output:
%   y_ref           : Generated reference signals
%                     (The number of targets, 2*The number of channels, Data length [sample])
%
% Reference:
%   [1] Z. Lin, C. Zhang, W. Wu, and X. Gao,
%       "Frequency Recognition Based on Canonical Correlation Analysis for SSVEP-Based BCI",
%       IEEE Trans. Biomed. Eng., 54(6), 1172-1176, 2007.
%   [2] G. Bin, X. Gao, Z. Yan, B. Hong, and S. Gao,
%       "An online multi-channel SSVEP-based brain-computer interface using a canonical correlation analysis method",
%       J. Neural Eng., 6 (2009) 046002 (6pp).
%
% Masaki Nakanishi, 28-Jul-2016
% Swartz Center for Computational Neuroscience, Institute for Neural
% Computation, University of California San Diego
% E-mail: masaki@sccn.ucsd.edu

if nargin < 3, error('stats:cca_reference:LackOfInput', 'Not enough input arguments.'); end
if ~exist('nHarms', 'var') || isempty(nHarms), nHarms = 3; end

nFreqs = length(listFreq);
tidx = (1:nSmpls)/fs;
for freq_i = 1:1:nFreqs
    tmp = [];
    for harm_i = 1:1:nHarms
        stimFreq = listFreq(freq_i);
        tmp = [tmp;...
            sin(2*pi*tidx*harm_i*stimFreq);...
            cos(2*pi*tidx*harm_i*stimFreq)];
    end % harm_i
    y_ref(freq_i, 1:2*nHarms, 1:nSmpls) = tmp;
end % freq_i