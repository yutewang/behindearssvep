function [ itr ] = itr(n, p, t)
% Calculate information transfer rate (ITR) for brain-computer interface (BCI) [2]
% function [ itr ] = itr(n, p, t)
% 
% Input:
%   n   : The numbers of targets
%   p   : Target identification accuracy (0 <= p <= 1) 
%   t   : Averaged time for a selection (s)
%
% Output:
%   itr : Information transfer rate (bits/min)   
%
% Reference:
%   [1] M. Cheng, X. Gao, S. Gao, and D. Xu,
%       "Design and Implementation of a Brain-Computer Interface With High Transfer Rates",
%       IEEE Trans. Biomed. Eng. 49, 1181-1186, 2002.
% 
% Masaki Nakanishi, 28-Jul-2016
% Swartz Center for Computational Neuroscience, Institute for Neural
% Computation, University of California San Diego
% E-mail: masaki@sccn.ucsd.edu

if nargin < 3, error('stats:itr:LackOfInput', 'Not enough input arguments.'); end

if p < 0 || p > 1
    error('stats:itr:BadInputValue', 'Accuracy need to be between 0 and 1.');
elseif  p == 1
    itr = log2(n)*60/t;
else
    itr = (log2(n) + p*log2(p) + (1-p)*log2((1-p)/(n-1)))*60/t;
end