%% x: the data need to be filtered (samples*channels)
% fs: sampling rate
function  y=bp(x,fs)
fs=fs/2;

Wp=[5/fs 50/fs];%3 7
Ws=[0.1/fs 75/fs];%1 1


Wn=0;
%[N,Wn]=cheb1ord(Wp,Ws,5,30);
[N,Wn]=cheb1ord(Wp,Ws,3,40);
[B,A] = cheby1(N,0.5,Wn);
y = filtfilt(B,A,x);
%y = filter(B,A,x);
%y=x;
%figure
%FREQZ(B,A)

 