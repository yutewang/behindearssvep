function results = test_trca(eeg, model, isEnsemble,filterType)
% Test phase of the task-related component analysis (TRCA)-based
% steady-state visual evoked potentials (SSVEPs) detection [1].
%
% function results = test_trca(eeg, model, isEnsemble)
%
% Input:
%   eeg             : Input eeg data
%                     (The number of targets, The number of channels, Data length [sample])
%   model           : Learning model for tesing phase of the ensemble TRCA-based method
%   isEnsemble      : 0: TRCA-based method, 1: Ensemble TRCA-based method (defult: 1)
%
% Output:
%   results         : The target estimated by this method
%
% See also:
%   train_trca.m
%
% Reference:
%   [1] M. Nakanishi, Y. Wang, X. Chen, Y. -T. Wang, X. Gao, and T.-P. Jung,
%       "Enhancing Detection of SSVEPs via Exploiting Inter-trial Information for a High-Speed Brain Speller",
%       , Vol., No., 2016.
%
% Masaki Nakanishi, 28-Jul-2016
% Swartz Center for Computational Neuroscience, Institute for Neural
% Computation, University of California San Diego
% E-mail: masaki@sccn.ucsd.edu

if ~exist('isEnsemble', 'var') || isempty(isEnsemble), isEnsemble = 1; end
if ~exist('model', 'var'), error('Training model based on TRCA is required. See train_trca().'); end

a = [1:model.nFBs].^(-1.25)+0.25;
filterOrder = 256;
dataShift = round(model.fs*0.5);

for targ_i = 1:1:model.nTargs
    test_tmp = squeeze(eeg(targ_i, :, :));
    for fb_i = 1:1:model.nFBs
%         testdata = filterbank(test_tmp, model.fs, fb_i);
%         testdata = test_tmp;
        testdata = test_tmp;
        save(['./Android/sbj1_PhoneDial0_TestData' num2str(targ_i)],'testdata');
        testdata = myFilter(test_tmp,filterOrder,filterType);
        testdataShifted = testdata(:,filterOrder+1:end);
%          testdata = testdata(:,1:end-dataShift);
        for class_i = 1:1:model.nTargs
            traindata =  squeeze(model.trains(class_i, fb_i, :, :));
            if ~isEnsemble
                w = squeeze(model.W(fb_i, class_i, :));
            else
                w = squeeze(model.W(fb_i, :, :))';
            end
            r(fb_i,class_i) = corr2(testdataShifted'*w, traindata'*w);
            [~, fftOut(1,class_i)] = max(abs(fft(testdataShifted'*w)));
            [~, fftOut(2,class_i)] = max(abs(fft(traindata'*w)));
            wtd = traindata'*w;
            %             save(['./testing/pd0814/PD0814_PhoneDial0_TrainingData' num2str(class_i)],'wtd');
            save(['./Android/sbj1_PhoneDial0_TrainingData' num2str(class_i)],'wtd');
        end % class_i
    end % fb_i
    %     save(['./testing/pd0814/PD0814_PhoneDial0_TestData' num2str(targ_i)],'testdata');
%     save(['./exp/sbj2_PhoneDial0_TestData' num2str(targ_i)],'testdata');
    rho = a*r;
    [rho' r'];
    [~, tau] = max(rho);
    results(targ_i) = tau;
    if(targ_i~=tau )
        if(~isempty(find((fftOut(2,:)-fftOut(1,:))==0)))
%             results(targ_i) = find((fftOut(2,:)-fftOut(1,:))==0,1)
        end
    end
end % targ_i