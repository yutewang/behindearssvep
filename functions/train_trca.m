function model = train_trca(eeg, fs, nFBs,filterType)
% Training phase of the task-related component analysis (TRCA)-based
% steady-state visual evoked potentials (SSVEPs) detection [1].
%
% function model = train_trca(eeg, fs, nFBs)
%
% Input:
%   eeg         : Input eeg data
%                               (The number of targets, The number of channels, Data length [sample])
%   fs          : Sampling frequency
%   nFBs        :The number of sub-bands
%
% Output:
%   model       : Learning model for tesing phase of the ensemble TRCA-based method
%       - traindata         : Training data decomposed into sub-band components by the filter bank analysis
%                             (The number of targets, The number of sub-bands, The number of channels, Data length [sample])
%       - W                 : Weight coefficients for electrodes which can be used as a spatial filter.
%       - nFBs              :The number of sub-bands
%       - fs                : Sampling frequency
%       - nTargs        : The number of targets
%
% See also:
%   test_trca.m
%
% Reference:
%   [1] M. Nakanishi, Y. Wang, X. Chen, Y. -T. Wang, X. Gao, and T.-P. Jung,
%       "Enhancing Detection of SSVEPs via Exploiting Inter-trial Information for a High-Speed Brain Speller",
%       , Vol., No., 2016.
%
% Masaki Nakanishi, 28-Jul-2016
% Swartz Center for Computational Neuroscience, Institute for Neural
% Computation, University of California San Diego
% E-mail: masaki@sccn.ucsd.edu

if nargin < 2, error('stats:cca_reference:LackOfInput', 'Not enough input arguments.'); end
if ~exist('nFBs', 'var') || isempty(nFBs), nFBs = 3; end

[nTargs, nChans, nSmpls, ~] = size(eeg);
filterOrder = 256;
%dataShift = round(fs*0.5);
trains = zeros(nTargs, nFBs, nChans, (nSmpls-filterOrder));%255
for targ_i = 1:1:nTargs
    eeg_tmp = squeeze(eeg(targ_i, :, :, :));
    for fb_i = 1:1:nFBs
%         eeg_tmp = filterbank(eeg_tmp, fs, fb_i);
        
        eeg_tmp = myFilter(eeg_tmp,filterOrder,filterType);
        eeg_tmp = eeg_tmp(:,filterOrder+1:end,:);%257 for 0.5sec
%         eeg_tmp = eeg_tmp(:,1:end-dataShift,:);%257 for 0.5sec
%         eeg_tmp = eeg_tmp-repmat(eeg_tmp
        trains(targ_i,fb_i,:,:) = squeeze(mean(eeg_tmp, 3));
        w_tmp = trca(eeg_tmp);
        W(fb_i, targ_i, 1:nChans) = w_tmp(:,1);
    end % fb_i
end % targ_i
model = struct('trains', trains, 'W', W, 'nFBs', nFBs, 'fs', fs, 'nTargs', nTargs);
w = squeeze(W);
% save( ['./testing/pd0814/PD0814_PhoneDial0_Weight' ],'w');
save( ['.\Android\sbj1_PhoneDial0_Weight'],'w');

function W = trca(eeg)
% Task-related component analysis (TRCA). This script was written based on
% the reference paper [1].
%
% function W = trca(eeg)
%
% Input:
%   eeg         : Input eeg data
%                 (The number of channels, Data length [sample], The number of trials)
%
% Output:
%   W           : Weight coefficients for electrodes which can be used as a spatial filter.
%
% Reference:
%   [1] H. Tanaka, T. Katura, H. Sato,
%         "Task-related component analysis for functional neuroimaging and application to near-infrared spectroscopy data",
%        NeuroImage, vol. 64, pp. 308-327, 2013.
%
% Masaki Nakanishi, 28-Jul-2016
% Swartz Center for Computational Neuroscience, Institute for Neural
% Computation, University of California San Diego
% E-mail: masaki@sccn.ucsd.edu

[nChans, nSmpls, nTrials]  = size(eeg);
S = zeros(nChans);
for trial_i = 1:1:nTrials
    for trial_j = 1:1:nTrials
        S = S + eeg(:,:,trial_i)*eeg(:,:,trial_j)';
    end % trial_j
end % trial_i
UX = reshape(eeg, nChans, nSmpls*nTrials);
UX = UX - repmat(mean(UX,2), 1, size(UX,2));
Q = UX*UX';
if(isnan(Q\S) )
    qq = 0;
end
[W,~] = eig(Q\S);
