% [ALLEEG EEG CURRENTSET ALLCOM] = eeglab;
addpath([cd '/functions']);
session = ['A' 'B' 'C' 'D'];
eopchTime = [-0.5 4];
if(strcmp(getenv('username'),'nan'))
    folderPath = 'C:\Users\UCSD\Desktop\behindearssvep\sccn.ucsd.edu\pub\YT\temp\dataset';


else
    folderPath = cd; %500G\BE:Wen's mac
end
fileName='';
minimumTrials = 1000;
subj = [1 2 4 5 6 7 8 9 10 11 12];
runNumber = [[8 1 6 9 5];[6 8 1 7 9];[10 6 8 2 7];[7 4 3 8 9]];
tg_label = {'9Hz','10Hz','11Hz','12Hz','13Hz'};

chans = {'occipital'; 'behindEar'};
chan_ab = {'OC';'BE'};
%% bdf to epoch-set
for ar = 1: length(chans),
    f_list=[];
    for sub=1%:length(subj),
        matfname = sprintf('%s%s%d%s%.2d%s%s%s',folderPath,'/subject',subj(sub),'/sbj',subj(sub),'_',chan_ab{ar},'.mat');
        %subj5,7,8,9,10,11,12
        if sum(subj(sub)==[5 7 8 9 10 11 12])>0,
            locfile = sprintf('%s%s%d%s',folderPath,'/subject',subj(sub),'/location.sfp');
        else
            locfile = sprintf('%s%s%d%s',folderPath,'/subject',subj(sub),'/location_topoplot.sfp');
        end
        EEG_sub=[];
        for rn = 1:size(runNumber,2)
            ALLEEG=[];
            for ss = 1:size(runNumber,1)
                EEG=[];
                fileName = sprintf('%s%s%s%s','session',session(ss), '_trial', num2str(runNumber(ss,rn)))
                bdfPath = sprintf('%s%s%d%s',folderPath,'/subject',subj(sub),'/');
                bdffname = [bdfPath fileName '.bdf'];
                if(strcmp(getenv('username'),'yute'))
                    EEG = pop_biosig( ['D:\ssvepRawData\subject' num2str(sub) '\' fileName '.bdf'], 'memorymapped','on');
                else
                    EEG = pop_biosig(bdffname,'memorymapped','off');
                end

                % add some decorations
                EEG.chanlocs = readlocs(locfile);
                EEG.setname = sprintf('%s%s%s%s','session',session(ss),':',tg_label{rn})
                EEG.subject = sprintf('%s%.2d','subject',subj(sub));
                
                % select chans corresponding to *.sfp
                a = struct2table(EEG.chanlocs);
                EEG = pop_select( EEG,'channel',cellstr(a.labels));
                
                % save continous set (w/ chanlocs)
                pop_saveset( EEG, 'filename',[fileName '.set'],'filepath',bdfPath);
                
                % select chns (occi / BE)
                EEG = pop_select(EEG,'channel',getChannelLabel(subj(sub),chans{ar}));
                
                EEG = pop_resample( EEG, 512);
                EEG = pop_rmbase( EEG, []);
                % epoch
                EEG = pop_epoch( EEG, {  '1'  }, eopchTime, 'newname', 'BDF file resampled epochs', 'epochinfo', 'yes');
                if(size(EEG.data,3)<minimumTrials)
                    minimumTrials = size(EEG.data,3);
                end
                pop_saveset( EEG, 'filename',[fileName '_ep.set'],'filepath',bdfPath);
                [ALLEEG, EEG, CURRENTSET] = eeg_store( ALLEEG, EEG, 0 );
                
            end
            % 5 runs for each frequency
            EEG_f = pop_mergeset( ALLEEG, [1:size(runNumber,1)], 0);
            EEG_sub(rn,:,:,1:size(EEG_f.data,3))= EEG_f.data(:,:,1:size(EEG_f.data,3));
        end
        save(matfname,'EEG_sub');
    end
end

%{
%% sanity testing

clear all;
load('subject12/sbj12_OC.mat');
for i=1:5
    a = squeeze(EEG_sub(i,:,:,:));
    figure;plot((0:2047)/4,abs(fft(bp(squeeze(mean(a,3))',512))));
    ylim([0 900]);xlim([0 50]);
    a=[];
end
%}

